﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    class Board
    {
        public List<Cell> cellList = new List<Cell>();

        public Board()
        {
            //GenerateMockCells();
            cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));
        }

        private void GenerateMockCells()
        {
            int X = Constants.cellColumnLeftDistance;
            int Y = Constants.cellRowTopDistance;
            GenerateBoard(X, Y);
            //File.WriteAllText(@"mockBoard.json", JsonConvert.SerializeObject(cellList));
        }
        private void GenerateBoard(int X, int Y)
        {
            int aux = 1;
            Y = 10 * Constants.cellHeight + 20;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Cell cell = new Cell
                    {
                        x = X,
                        y = Y,
                        //propertyName = "Proprietatea " + i,
                        cellNumber = aux
                    };
                    aux++;
                    cellList.Add(cell);
                    X += Constants.cellWidth + 2;
                }
                Y -= Constants.cellHeight + 2;
                X = Constants.cellColumnLeftDistance;
            }
        }

        public void Show(Form f)
        {
            foreach (Cell cell in cellList)
            {
                cell.Show(f);
            }
            f.Refresh();
        }
    }
}
