﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeAndLadder
{
    static class Constants
    {
        static public int cellHeight = 70;
        static public int cellWidth = 70;
        internal static Color cellBackgroundColor = Color.Lime;
        internal static int cellColumnLeftDistance = 10;
        internal static int cellRowTopDistance = 10;
    }
}
