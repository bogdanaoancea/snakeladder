﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    class Cell
    {
        public int x, y;
        public int cellNumber;
        public bool snake, ladder;
        public Panel background;
        public Panel playerPanel;

        private Panel SetBackgroudPanelProperties()
        {
            Panel backgroundPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Left = x,
                Top = y,
                Height = Constants.cellHeight,
                Width = Constants.cellWidth
            };
            return backgroundPanel;
        }

        private Panel SetPlayerPanelProperties()
        {
            Panel colorPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                Dock = DockStyle.Top,
                Height = Constants.cellHeight / 5
            };
            return colorPanel;
        }

        private Panel SetCellNumberProperties()
        {
            Panel cellNumberPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Dock = DockStyle.Bottom,

            };

            Label cellNumberLabel = new Label();
            cellNumberLabel.Text = cellNumber.ToString();
            cellNumberLabel.Dock = DockStyle.Bottom;
            cellNumberLabel.TextAlign = ContentAlignment.MiddleCenter;
            cellNumberLabel.Font = new Font("Arial", 15);

            cellNumberPanel.Controls.Add(cellNumberLabel);

            return cellNumberPanel;
        }

        private Panel SetPropertiesPanelProperties()
        {
            Panel propertiesPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Dock = DockStyle.Top,
                Height = Constants.cellHeight / 2
            };

            Label cellProperties = new Label();
            cellProperties.Text = "Snake: " + snake.ToString() + '\n' + "Ladder: " + ladder.ToString();
            cellProperties.Dock = DockStyle.Fill;
            cellProperties.TextAlign = ContentAlignment.MiddleCenter;
            cellProperties.Font = new Font("Arial", 8);

            propertiesPanel.Controls.Add(cellProperties);

            return propertiesPanel;
        }
        internal void Show(Form f)
        {
            background = SetBackgroudPanelProperties();


            Panel propertiesPanel = SetPropertiesPanelProperties();
            background.Controls.Add(propertiesPanel);

            playerPanel = SetPlayerPanelProperties();
            background.Controls.Add(playerPanel);

            Panel cellNumberPanel = SetCellNumberProperties();
            background.Controls.Add(cellNumberPanel);

            f.Controls.Add(background);
        }
    }
}
